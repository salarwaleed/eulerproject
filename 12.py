def triNum(num):
    sum=0
    for i in range(1,num+1):
        sum=sum+i
    return sum

def factor(num):
    lst=[]
    for i in range(1,num+1):
        if num%i==0:
            lst.append(i)
    return len(lst)

print(triNum(7))
print(factor(triNum(7)))

i=5
while factor((triNum(i)))<500:
    i=i+1
print(i)
