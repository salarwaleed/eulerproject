def isEven(num):
    if num%2==1:
        return False
    else:
        return True


def chainLen(num):
    count=0
    while num!=1:
        if isEven(num):
            num=num/2
            count+=1
        else:
            num=3*num+1
            count+=1
    return count

max=0
for i in range(1,1000000):
    len=chainLen(i)
    if len>max:
        max=len
        maxIndex=i


print(i)
